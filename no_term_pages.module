<?php

// There's actually no easy way to avoid links to taxonomy term pages. So we just
// remove the pages. It's the site builder's responsability to avoid broken links
// by using correct formatters.

/**
 * Implements hook_menu_alter().
 */
function no_term_pages_menu_alter(&$items) {
  foreach ($items as $path => &$item) {
    if (isset($item['page callback'])) {
      if ($item['page callback'] == 'taxonomy_term_page') {
        $item['page callback'] = 'no_term_pages_taxonomy_term_page_router';
      }
      if ($item['page callback'] == 'taxonomy_term_feed') {
        $item['page callback'] = 'no_term_pages_taxonomy_term_feed_router';
      }
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function no_term_pages_form_taxonomy_form_vocabulary_alter(&$form, &$form_state) {
  $vocabulary = $form_state['vocabulary'];
  $form['term_pages'] = array(
    '#type' => 'checkbox',
    '#title' => t('Each term of this vocabulary has its page (a listing of related nodes by default)'),
    '#description' => t('Uncheck to remove term pages'),
    '#default_value' => isset($vocabulary->term_pages) ? $vocabulary->term_pages : 1,
  );
  $form['term_feeds'] = array(
    '#type' => 'checkbox',
    '#title' => t('Each term of this vocabulary has its feed'),
    '#description' => t('Uncheck to remove term feeds'),
    '#default_value' => isset($vocabulary->term_feeds) ? $vocabulary->term_feeds : 1,
  );
}

/**
 * Returns the term page only if it should be available.
 */
function no_term_pages_taxonomy_term_page_router($term) {
  $vocabulary = taxonomy_vocabulary_load($term->vid);
  if (!$vocabulary->term_pages) {
    drupal_not_found();
    return;
  }
  return taxonomy_term_page($term);
}

/**
 * Returns the term feed only if it should be available.
 */
function no_term_pages_taxonomy_term_feed_router($term) {
  $vocabulary = taxonomy_vocabulary_load($term->vid);
  if (!$vocabulary->term_feeds) {
    drupal_not_found();
    return;
  }
  return taxonomy_term_feed($term);
}
